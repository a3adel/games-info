plugins {
    id(BuildPlugins.ANDROID)
    id(BuildPlugins.KOTLIN_ANDROID)
}

android {
    compileSdk = 32


}

dependencies {

    implementation(Deps.COROUTINES)
    implementation(Deps.PAGING_COMMON)
    implementation(Deps.KOIN)
}