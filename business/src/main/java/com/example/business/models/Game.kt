package com.example.business.models

data class Game(
    val id: Int,
    val name: String,
    val rating: Float,
    val backgroundImage: String?,
    val ratingPercent:Float,
    val releaseDate:String
)
