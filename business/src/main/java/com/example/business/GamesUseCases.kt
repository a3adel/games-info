package com.example.business

data class GamesUseCases(
    val fetch: FetchGamesUseCase,
    val get: GetGamesDetailsUseCase
)