package com.example.business.di

import com.example.business.FetchGamesUseCase
import com.example.business.GamesUseCases
import com.example.business.GetGamesDetailsUseCase
import org.koin.dsl.module

val gamesModules = module {
    factory {
        FetchGamesUseCase(gamesRepository = get())
    }
    factory { GetGamesDetailsUseCase(repository = get()) }
    factory {
        GamesUseCases(fetch = get(), get = get())
    }
}