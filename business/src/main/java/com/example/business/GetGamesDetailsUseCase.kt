package com.example.business

import com.example.business.models.GameDetails

class GetGamesDetailsUseCase(private val repository: GamesRepository) {
    suspend operator fun invoke(id:Int):GameDetails{
      return  try {
            repository.getGame(id)
        }catch (exc:Exception){
            // in here its better to have a specific custom app error but to save time I would just
            // throw the previous exception
            throw exc
        }
    }
}