package com.example.business

class FetchGamesUseCase(private val gamesRepository: GamesRepository) {
    suspend operator fun invoke() =
        gamesRepository.fetchGames()

}