package com.example.business

import androidx.paging.PagingData
import com.example.business.models.Game
import com.example.business.models.GameDetails
import kotlinx.coroutines.flow.Flow

interface GamesRepository {

    suspend fun fetchGames(): Flow<PagingData<Game>>

    suspend fun getGame(gameId: Int): GameDetails
}