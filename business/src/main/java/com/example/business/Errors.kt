package com.example.business

object Errors {
    class NetworkError : Exception()
    class UserNotAuthorized : Exception()
    class PageNotFound:Exception()
    class GeneralError():Exception()
}