object Versions {
    const val GRADLE_PLUGIN = ""
    const val KOTLIN = ""
    const val APP_COMPAT = "1.5.0"
    const val JUNIT = ""
    const val MIN_SDK = 21
    const val COMPILE_SDK = 32
    const val TARGET_SDK = 32
    const val ANDROIDX_CORE = "1.8.0"
    const val MATERIAL = "1.6.1"
    const val RETROFIT = "2.9.0"
    const val MOSHI = "1.13.0"
    const val COROUTINES = "1.3.9"
    const val KOIN_VERSION = "3.2.0"
    const val PAGING_VERSION = "3.1.1"
    const val LIFE_CYCLE_VERSION = "2.6.0-alpha01"
    const val NAVIGATION = "2.5.1"
    const val FRAGMENT = "1.5.2"

    const val GLIDE_VERSION = "4.13.2"
}