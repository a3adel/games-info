
# Games Info

This project is a simple List/Details applications for games using the rawg API


## Architecture
This Application is based on Clean Architecture by applying modular Architecture the main modules in the Application are
- ### data module (depends on business and services)
    this module is responsible for fetching the data from the differnet data sources (and I applied the network Data Source in this application), its also responsible for the repository pattern where the data sources are injected to the repo
- ### business (depends on nothing)
    this module is responsible for the domain layer and any business logic 
- ### core_ui (depends on nothing)
    this module is responsible for helper classes for the ui and base classes BaseViewModel for example, custom UI components and custom clickListeners 
- ### services ( depends on nothing)
    thiss module is responsible for providing different services for the application for example NetworkConnectivityService or LocationServices if we later on decided to add location services 
- ### la3eeb ( depends on business)
    this module contains the actual ui for the application (Activites, Fragments and View Models)


## Tech Stack
this application uses the following libraries:-
- Couroutines 
- Flows
- Retrofit
- Glide
- Moshi
- Koin for Depenancy injection
- Paging3

Also I used these 2 repos as a reference 
- [vamadain](https://github.com/vmadalin/android-modular-architecture) repo which I inspired the modularization from
- [Google architecture sample](https://https://github.com/android/architecture-samples/)

## Installation

- download the project from gitlab
- create a file named Constants instead of Constants.kts.template (which is a guide instead of Constants as Constants is not pushed toe the git)
  qnd replace the commented values with the real values
