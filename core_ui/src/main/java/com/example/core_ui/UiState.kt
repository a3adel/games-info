package com.example.core_ui

data class UiState<out T>(
    val isLoading: Boolean,
    val data: T? = null,
    val errorMessage: String? = null
)
