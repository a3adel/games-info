package com.example.core_ui

fun interface OnRecyclerViewItemClickListener<in T> {
    fun onClick(item: T)
}