plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("androidx.navigation.safeargs.kotlin")

}

android {
    compileSdk = 32

    defaultConfig {
        applicationId = "com.example.la3eeb"
        minSdk = 21
        targetSdk = 32
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    buildFeatures {
        dataBinding = true
        viewBinding = true
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

}

dependencies {

    implementation("androidx.appcompat:appcompat:1.5.0")
    implementation("com.google.android.material:material:1.6.1")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation("io.insert-koin:koin-android:3.2.0")
    implementation("io.insert-koin:koin-core:3.2.0")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.LIFE_CYCLE_VERSION}")
    implementation(Deps.KOIN)
    implementation(Deps.KOIN_ANDROID)
    api(Deps.NAVIGATION_FRAGMENT)
    api(Deps.NAVIGATION_UI)
    implementation(Deps.GLIDE)
    implementation(Deps.PAGING_ANDROID_RUNTIME)
    annotationProcessor(Deps.GLIDE_COMPILER)
    implementation(project(mapOf("path" to ":business")))
    implementation(project(mapOf("path" to ":core_data")))
    implementation(project(mapOf("path" to ":core_ui")))
    implementation(project(mapOf("path" to ":services")))
}