package com.example.la3eeb.di

import com.example.la3eeb.games.game_details.GameDetailsViewModel
import com.example.la3eeb.games.games_list.GamesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModules = module {
    viewModel {
        GamesViewModel(gamesUseCases = get())
    }
    viewModel {
        GameDetailsViewModel(getGamesDetailsUseCase = get())
    }
}