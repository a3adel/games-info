package com.example.la3eeb

import android.app.Application
import com.example.business.di.gamesModules
import com.example.core_data.di.NetworkServicesModules
import com.example.la3eeb.di.viewModelModules
import com.example.services.ServicesModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class La3eebApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@La3eebApp)
            modules(
                listOf(
                    gamesModules,
                    NetworkServicesModules,
                    ServicesModules,
                    viewModelModules
                )
            )
        }
    }
}