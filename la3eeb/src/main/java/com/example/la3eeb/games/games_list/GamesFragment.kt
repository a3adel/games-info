package com.example.la3eeb.games.games_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.core_ui.ProgressBarDialog
import com.example.core_ui.VerticalSpaceItemDecoration
import com.example.la3eeb.databinding.FragmentGamesBinding
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel


class GamesFragment : Fragment() {
    lateinit var binding: FragmentGamesBinding
    private val viewModel by viewModel<GamesViewModel>()
    private val pagingAdapter = GameAdapter(GameComparator)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGamesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.gamesRv.addItemDecoration(VerticalSpaceItemDecoration(20))
        pagingAdapter.setOnItemClickListener { game ->
            findNavController().navigate(
                GamesFragmentDirections.actionGamesFragmentToGameDetailsFragment(
                    game.id
                )
            )
        }
        binding.gamesRv.adapter = pagingAdapter
        val progressBarDialog = ProgressBarDialog(requireContext())
        viewLifecycleOwner.lifecycleScope.launch {

            viewModel.fetchGames().collectLatest { state ->
                if(state.isLoading)
                    progressBarDialog.show()
                else
                    progressBarDialog.dismiss()
                state.data?.let {
                    pagingAdapter.submitData(it)
                }
            }
        }
    }
}