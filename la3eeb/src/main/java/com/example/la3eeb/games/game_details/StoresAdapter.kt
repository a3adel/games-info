package com.example.la3eeb.games.game_details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.la3eeb.databinding.ItemStoreBinding
import com.example.la3eeb.models.UiStoreEntity

class StoresAdapter : RecyclerView.Adapter<StoresAdapter.StoreViewHolder>() {
    private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<UiStoreEntity>() {
        override fun areItemsTheSame(oldItem: UiStoreEntity, newItem: UiStoreEntity): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: UiStoreEntity, newItem: UiStoreEntity): Boolean =
            oldItem == newItem
    }
    private val mDiffer: AsyncListDiffer<UiStoreEntity> = AsyncListDiffer(this, DIFF_CALLBACK)

    inner class StoreViewHolder(private val binding: ItemStoreBinding) :
        RecyclerView.ViewHolder(binding.root) {
            fun bind(store:UiStoreEntity){
                binding.storeNameTextView.text = store.name

            }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreViewHolder {
        val binding = ItemStoreBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return StoreViewHolder(binding)
    }

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {

    }

    override fun getItemCount(): Int =
        mDiffer.currentList.size

    fun submitList(list: List<UiStoreEntity>) = mDiffer.submitList(list)


}