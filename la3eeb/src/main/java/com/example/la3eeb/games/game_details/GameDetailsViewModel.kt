package com.example.la3eeb.games.game_details

import androidx.lifecycle.viewModelScope
import com.example.business.GetGamesDetailsUseCase
import com.example.core_ui.BaseViewModel
import com.example.core_ui.UiState
import com.example.la3eeb.models.UiGameDetailsEntity
import com.example.la3eeb.models.toUiModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.stateIn

class GameDetailsViewModel(private val getGamesDetailsUseCase: GetGamesDetailsUseCase) :
    BaseViewModel() {


    suspend fun getGame(id: Int): Flow<UiState<UiGameDetailsEntity>> {
        return flow {
            emit(
                try {
                    val gameDetails = getGamesDetailsUseCase(id).toUiModel()
                    UiState(false, gameDetails)
                } catch (exc: Exception) {
                    //the error message should be localized based on error
                    UiState(false, null, exc.toString())
                }
            )
        }.stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5000L),
            initialValue = UiState(true)
        )
    }
}