package com.example.la3eeb.games.games_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.core_ui.OnRecyclerViewItemClickListener
import com.example.la3eeb.databinding.ItemGameBinding
import com.example.la3eeb.models.UiGameEntity

class GameAdapter(diffCallback: DiffUtil.ItemCallback<UiGameEntity>) :
    PagingDataAdapter<UiGameEntity, GameAdapter.GameViewHolder>(diffCallback) {
    private var recyclerViewItemClickListener: OnRecyclerViewItemClickListener<UiGameEntity>? = null
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GameViewHolder {
        val binding = ItemGameBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GameViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GameViewHolder, position: Int) {
        val item = getItem(position)
        // Note that item may be null. ViewHolder must support binding a
        // null item as a placeholder.
        holder.bind(item)
    }

    fun setOnItemClickListener(recyclerViewItemClickListener: OnRecyclerViewItemClickListener<UiGameEntity>) {
        this@GameAdapter.recyclerViewItemClickListener = recyclerViewItemClickListener
    }

    inner class GameViewHolder(private val binding: ItemGameBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(game: UiGameEntity?) {
            if (game != null) {
                binding.gameNameTextView.text = game.name
                binding.releaseDateTextView.text = game.releaseDate
                binding.ratingTextView.text = "${game.ratingPercent*100}%"
                binding.root.setOnClickListener {
                    recyclerViewItemClickListener?.onClick(game)
                }
                if (game?.backgroundImage != null)
                    Glide.with(binding.gameLogo.context).load(game?.backgroundImage)
                        .into(binding.gameLogo)
            }
        }
    }
}

object GameComparator : DiffUtil.ItemCallback<UiGameEntity>() {
    override fun areItemsTheSame(oldItem: UiGameEntity, newItem: UiGameEntity): Boolean {
        // Id is unique.
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: UiGameEntity, newItem: UiGameEntity): Boolean {
        return oldItem == newItem
    }
}