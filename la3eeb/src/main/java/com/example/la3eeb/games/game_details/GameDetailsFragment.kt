package com.example.la3eeb.games.game_details

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.example.core_ui.ProgressBarDialog
import com.example.la3eeb.databinding.FragmentGameDetailsBinding
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel


class GameDetailsFragment : Fragment() {
    private val viewModel by viewModel<GameDetailsViewModel>()
    private lateinit var binding: FragmentGameDetailsBinding
    val args by navArgs<GameDetailsFragmentArgs>()
    lateinit var progressBarDialog: ProgressBarDialog
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentGameDetailsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        progressBarDialog = ProgressBarDialog(requireContext())
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.getGame(args.id).collectLatest {
                if (it.isLoading)
                    progressBarDialog.show()
                else
                    progressBarDialog.dismiss()
                if (it.data != null) {
                    if(it.data?.backgroundImage!=null)
                        Glide.with(requireContext()).load(it.data?.backgroundImage).into(binding.gameBackgroundImageView)
                    binding.gameNameTextView.text = it.data?.name
                    if (it.data?.description != null)
                        binding.descriptionTextView.text = it.data?.description


                }
                if(it.errorMessage!=null)
                    Toast.makeText(requireContext(),it.errorMessage,Toast.LENGTH_LONG).show()
            }

        }
    }

}