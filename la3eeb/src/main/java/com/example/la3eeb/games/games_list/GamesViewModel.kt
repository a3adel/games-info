package com.example.la3eeb.games.games_list

import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.map
import com.example.business.GamesUseCases
import com.example.core_ui.BaseViewModel
import com.example.core_ui.UiState
import com.example.la3eeb.models.UiGameEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn

class GamesViewModel(private val gamesUseCases: GamesUseCases) : BaseViewModel() {

    suspend fun fetchGames(): Flow<UiState<PagingData<UiGameEntity>>> {
        return gamesUseCases.fetch().map {
            UiState(
                false,
                it.map {
                    UiGameEntity(
                        id = it.id,
                        name = it.name,
                        rating = it.rating,
                        it.backgroundImage,
                        ratingPercent = it.ratingPercent,
                        releaseDate = it.releaseDate
                    )
                },
                null
            )
        }.stateIn(
            viewModelScope,
            started = SharingStarted.WhileSubscribed(5000L),
            initialValue = UiState(true)
        )
    }

}