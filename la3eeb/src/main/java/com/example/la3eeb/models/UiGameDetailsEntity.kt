package com.example.la3eeb.models

import com.example.business.models.GameDetails

data class UiGameDetailsEntity(
    val id: Int,
    val name: String,
    val rating: Float,
    val backgroundImage: String?,
    val ratingPercent: Float,
    val releaseDate: String,
    val description:String?
)

fun GameDetails.toUiModel(): UiGameDetailsEntity =
    UiGameDetailsEntity(
        id = id,
        name = name,
        rating = rating,
        backgroundImage = backgroundImage,
        ratingPercent = ratingPercent,
        releaseDate = releaseDate,
        description = description
    )

