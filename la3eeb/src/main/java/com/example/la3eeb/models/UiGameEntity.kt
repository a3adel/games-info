package com.example.la3eeb.models

data class UiGameEntity(
    val id: Int,
    val name: String,
    val rating: Float,
    val backgroundImage: String?,
    val ratingPercent: Float,
    val releaseDate:String
)