package com.example.la3eeb.models

data class UiStoreEntity(
    val id: Int,
    val name: String,
    val image: String?
)
