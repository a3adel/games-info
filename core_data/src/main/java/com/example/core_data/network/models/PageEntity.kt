package com.example.core_data.network.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
open class PageEntity<T>(
    @field:Json(name = "count") val count: Int,
    @field:Json(name = "previous") val previousPage: String?,
    @field:Json(name = "next") val nextPage: String?,
    @field:Json(name = "results") val results: List<T>
)