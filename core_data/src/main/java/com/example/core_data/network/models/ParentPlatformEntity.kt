package com.example.core_data.network.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ParentPlatformEntity(@field:Json(name = "platform") val platform: Platform)
