package com.example.core_data.network.models

import com.squareup.moshi.Json

@Json
data class PlatformInfoEntity(
    @field:Json(name = "requirements_en") val englishRequirements: String?,
    @field:Json(name = "requirements_en") val releasedDate: String
)
