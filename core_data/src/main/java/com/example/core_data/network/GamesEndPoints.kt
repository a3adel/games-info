package com.example.core_data.network

import com.example.core_data.network.models.GameDetailsEntity
import com.example.core_data.network.models.GameEntity
import com.example.core_data.network.models.PageEntity
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GamesEndPoints {
    @GET("api/games")
    suspend fun fetchGamesList(
        @Query("page_size") pageSize: Int,
        @Query("page") page: Int
    ): Response<PageEntity<GameEntity>>

    @GET("api/games/{id}")
    suspend fun getGame(@Path("id") id: Int): Response<GameDetailsEntity>
}