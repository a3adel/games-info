package com.example.core_data.network.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GameDetailsEntity(
    @field:Json(name = "id") val id: Int,
    @field:Json(name = "name") val name: String,
    @field:Json(name = "released") val releaseDate: String,
    @field:Json(name = "background_image") val backgroundImage: String?,
    @field:Json(name = "rating") val rating: Float,
    @field:Json(name = "rating_top") val topRating: Float,
    @field:Json(name = "description") val description: String?
)
