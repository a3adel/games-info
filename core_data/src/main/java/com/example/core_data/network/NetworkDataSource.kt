package com.example.core_data.network

import com.example.core_data.network.models.GameDetailsEntity
import com.example.core_data.network.models.GameEntity
import com.example.core_data.network.models.PageEntity

interface NetworkDataSource {
    suspend fun fetchGames(page: Int, pageSize: Int = 10): Result<PageEntity<GameEntity>>
    suspend fun getGame(gameId: Int): Result<GameDetailsEntity>
}