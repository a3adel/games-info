package com.example.core_data.network

import com.example.business.Errors
import com.example.core_data.network.models.GameDetailsEntity
import com.example.core_data.network.models.GameEntity
import com.example.core_data.network.models.PageEntity
import com.example.services.ConnectivityService

class NetworkDataSourceImpl(
    private val connectivityService: ConnectivityService,
    private val gamesEndPoints: GamesEndPoints
) : NetworkDataSource {
    override suspend fun fetchGames(page: Int, pageSize: Int): Result<PageEntity<GameEntity>> {
        return if (connectivityService.isNetworkAvailable()) {
            val response = gamesEndPoints.fetchGamesList(pageSize = pageSize, page = page)
            if (response.isSuccessful && response.body() != null)
                Result.Success(response.body()!!)
            else {
                if (response.code() == 404)
                    Result.Error(Errors.PageNotFound())
                else
                    Result.Error(Errors.GeneralError())

            }
        } else {
            Result.Error(Errors.NetworkError())
        }
    }

    override suspend fun getGame(gameId: Int): Result<GameDetailsEntity> {
        return if (connectivityService.isNetworkAvailable()) {
            val response = gamesEndPoints.getGame(id = gameId)
            if (response.isSuccessful && response.body() != null)
                Result.Success(response.body()!!)
            else {
                if (response.code() == 404)
                    Result.Error(Errors.PageNotFound())
                else
                    Result.Error(Errors.GeneralError())

            }
        } else {
            Result.Error(Errors.NetworkError())
        }
    }

}