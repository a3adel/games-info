package com.example.core_data.di

import com.example.business.GamesRepository
import com.example.core_data.Constants
import com.example.core_data.Constants.BASE_URL
import com.example.core_data.GamesPagingSource
import com.example.core_data.network.GamesEndPoints
import com.example.core_data.network.NetworkDataSource
import com.example.core_data.network.NetworkDataSourceImpl
import com.example.core_data.repos.GamesRepositoryImpl
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

val NetworkServicesModules = module {
    single<GamesEndPoints> {
        getRetrofitClient().create(GamesEndPoints::class.java)
    }

    single<NetworkDataSource> {
        NetworkDataSourceImpl(
            connectivityService = get(),
            gamesEndPoints = get()
        )
    }

    single {
        GamesPagingSource(networkDataSource = get())
    }
    single<GamesRepository> {
        GamesRepositoryImpl(gamesPagingSource = get(), networkDataSource = get())
    }
}

private fun getRetrofitClient() = Retrofit.Builder()
    .baseUrl(BASE_URL)
    .client(getHttpClient())
    .addConverterFactory(
        MoshiConverterFactory.create(
            Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()
        )
    )
    .build()

private fun getHttpClient(): OkHttpClient {
    val client = OkHttpClient.Builder()
    client.addInterceptor { chain ->
        val request = chain.request()
        val originalUrl = request.url()
        val httpUrl = originalUrl.newBuilder()
            .addQueryParameter("key", Constants.API_SECRET)
            .build()
        val requestBuilder = request.newBuilder().url(httpUrl)

        chain.proceed(requestBuilder.build())
    }
    client.connectTimeout(30, TimeUnit.SECONDS); // connect timeout
    client.readTimeout(30, TimeUnit.SECONDS);
    return client.build()
}