package com.example.core_data.repos

import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.example.business.GamesRepository
import com.example.business.models.GameDetails
import com.example.core_data.GamesPagingSource
import com.example.core_data.network.NetworkDataSource
import com.example.core_data.network.Result

class GamesRepositoryImpl(
    private val gamesPagingSource: GamesPagingSource,
    private val networkDataSource: NetworkDataSource
) : GamesRepository {
    override suspend fun fetchGames() =
        Pager(PagingConfig(pageSize = 10)) {
            gamesPagingSource
        }.flow

    override suspend fun getGame(gameId: Int): GameDetails {
        return when (val result = networkDataSource.getGame(gameId)) {
            is Result.Success -> GameDetails(
                id = result.data.id,
                name = result.data.name,
                description = result.data.description,
                releaseDate = result.data.releaseDate,
                ratingPercent = result.data.rating / result.data.topRating,
                backgroundImage = result.data.backgroundImage,
                rating = result.data.rating
            )
            is Result.Error ->
                throw result.exception
        }

    }
}