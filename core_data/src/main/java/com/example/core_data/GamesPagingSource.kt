package com.example.core_data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.business.models.Game
import com.example.core_data.network.NetworkDataSource
import com.example.core_data.network.models.GameEntity
import com.example.core_data.network.models.PageEntity

class GamesPagingSource(private val networkDataSource: NetworkDataSource) :
    PagingSource<Int, Game>() {
    override fun getRefreshKey(state: PagingState<Int, Game>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Game> {
        val nextPageNumber = params.key ?: 1
        return when (val response = networkDataSource.fetchGames(page = nextPageNumber)) {
            is com.example.core_data.network.Result.Success<PageEntity<GameEntity>> -> {
                LoadResult.Page(
                    data = response.data.results.map {
                        Game(
                            id = it.id,
                            name = it.name,
                            rating = it.rating,
                            ratingPercent = (it.rating / it.topRating),
                            backgroundImage = it.backgroundImage,
                            releaseDate = it.releaseDate
                        )
                    },
                    nextKey = nextPageNumber+1,
                    prevKey = null
                )
            }
            is com.example.core_data.network.Result.Error -> {
                LoadResult.Error(response.exception)
            }
        }

    }
}