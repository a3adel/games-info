package com.example.services

import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val ServicesModules = module {
    single { ConnectivityService(context = androidContext()) }
}